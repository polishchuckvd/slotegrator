<?php
/**
 * Load application environment from .env file
 */
$dotenv = \Dotenv\Dotenv::create(dirname(__FILE__));
$dotenv->load();