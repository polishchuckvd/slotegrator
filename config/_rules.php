<?php

use yii\web\GroupUrlRule;

return [
    'GET api/add' => 'api/default/add',
    'GET api/feed' => 'api/default/feed',
    'GET api/remove' => 'api/default/remove',
    [
        'class'       => GroupUrlRule::class,
        'prefix'      => '/',
        'routePrefix' => 'user/manage',
        'rules'       => [
            '/' => '/',
            'create' => 'create',
            'delete' => 'delete',
            'create-validate' => 'create-validate',
        ]
    ],

];
