<?php

use yii\db\Migration;

/**
 * Class m190905_093124_create_table_twitter_user
 */
class m190905_093124_create_table_twitter_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $sql = "CREATE TABLE `twitter_user` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`id`)
        )
        COLLATE='utf8_general_ci'
        ;";

        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $sql = "DROP TABLE `twitter_user`";

        \Yii::$app->db->createCommand($sql)->execute();
    }
}
