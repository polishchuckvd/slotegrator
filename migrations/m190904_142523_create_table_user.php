<?php

use yii\db\Migration;

/**
 * Class m190904_142523_create_table_user
 */
class m190904_142523_create_table_user extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $sql = "CREATE TABLE IF NOT EXISTS `user` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `login` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `created_at` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        `secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `login` (`login`)
      ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $sql = "DROP TABLE `user`";

        \Yii::$app->db->createCommand($sql)->execute();
    }

}
