<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>

<div class="cabinet__create-user">
    <!-- <div class="modal__window-close">&#10006;</div> -->
    <?php
    $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'method' => 'post',
//        'action' => Url::to(['/cart/cart/checkouts']),
        'validationUrl' => Url::to(['/user/manage/create-validate']),    
    ]);
    ?>
        <h2 for="name3" class="cabinet__title">Регистрация</h2>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($modelUser, 'user_id')->input('text') ?>
                <?= $form->field($modelUser, 'login')->input('text') ?>
            </div>
        </div>
        <input type="submit" class="cabinet__create-user-btn btn btn_bg-blue" value="Сохранить" style="float: right">
    <?php ActiveForm::end(); ?>
</div>