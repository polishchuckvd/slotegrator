<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'sidebar-users';
//$this->params['breadcrumbs'][] = $this->title;
?>   
<section class="cabinet__admin">

    <div class="cabinet__admin-table-wrap">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'user_id',
                    'label' => 'ID',
                ],
                [
                    'attribute' => 'login',
                    'label' => 'Логин',
                ],
                [
                    'attribute' => 'secret',
                    'label' => 'Secret',
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Зарегестрирован',
                    'value' => function ($model) {

                        return Yii::$app->formatter->asDateTime($model->created_at, 'php:d-m-Y H:m');
                    },
                ],
                [
                    'class' => ActionColumn::class,
                    'header' => 'Управление',
                    'template' => '{delete}',
                ],
            ],
        ]);
        ?>


        <div class="cabinet__admin-btn">
            <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn_transparent btn_transparent_border-blue btn-success']) ?>
        </div>

</section>
