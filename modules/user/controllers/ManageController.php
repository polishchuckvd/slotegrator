<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\User;
use app\modules\user\models\Manage;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use app\modules\user\components\repositories\UserRepository;

/**
 * ManageController implements the CRUD actions for User model.
 */
class ManageController extends Controller {

    private $userRepository;
    
    private $manage;

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function __construct($id, $module, UserRepository $userRepository, Manage $manage, $config = []) {

        $this->manage = $manage;
        $this->userRepository = $userRepository;

        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new Manage();
        $dataProvider = $this->manage->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Verify user creation form
     * @return mixed
     */
    public function actionCreateValidate() {
        if (\Yii::$app->request->isAjax) {
            $modelUser = new User();

            if ($modelUser->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($modelUser);
            }

        }

        throw new HttpException(500);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        
        $modelUser = new User();
        
        if ($modelUser->load(Yii::$app->request->post()) && $modelUser->validate()) {
            if ($modelUser->signup()) {
                return $this->redirect(['/']);
            }
            
            throw new NotFoundHttpException('Не удалось сохранить пользователя!');
        }

        return $this->render('create', [
            'modelUser' => $modelUser,
        ]);
    }


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['/']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
