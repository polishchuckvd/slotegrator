<?php

namespace app\modules\api\controllers;

use Yii;
use yii\rest\Controller;
use app\modules\api\components\services\ErrorService;
use app\modules\api\models\TwitterUser;
use app\modules\api\models\TwitterApi;
use app\modules\api\components\services\ApiService;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
     /**
     * GET request params
     * @var array
     */
    private $params;

    /**
     * User acces token
     * @var string
     */
    private $secret;

    /**
     * @var app\components\repositories\UserRepository
     */
    private $errorService;

    /**
     * @var ApiService
     */
    private $apiService;

    /**
     * {@inheritdoc}
     */
    public function __construct($id, $module, ErrorService $errorService,
            ApiService $apiService, $config = array()) {
        parent::__construct($id, $module, $config);
        
        $this->errorService = $errorService;
        $this->apiService = $apiService;

        $this->params = Yii::$app->request->get();
        $this->secret = $this->params['secret'];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::class,
                'formatParam' => '_format',
                'formats' => [
                    'json' => \yii\web\Response::FORMAT_JSON
                ],
            ]
        ];
    }

    /**
     * Return twets
     */
    public function actionFeed() {
        $twitterUsers = TwitterUser::find()->asArray()->all();
        $twitterUsers = ArrayHelper::map($twitterUsers, 'id', 'name');
        
        $this->apiService->setTwitterNames($twitterUsers);
        
        return $this->apiService->getTwets();
    }

    /**
     * Remove twitter user
     * Required GET parameters ['id', 'user', 'secret']
     */
    public function actionRemove() {
        $this->errorService->setParams(['id', 'user', 'secret']);
        
        if(!empty($this->errorService->getResponce())) {
            return $this->errorService->getResponce();
        }
        
        $model = TwitterUser::findOne([
            'name' => $this->params['user'],
            'id' => $this->params['id']
        ]);
        
        return (isset($model) 
                && $model->delete()) ? true : ['error' => 'Internal error'];
 
    }

    /**
     * Add twitter user
     * Required GET parameters ['id', 'user', 'secret']
     */
    public function actionAdd() {
        $this->errorService->setParams(['id', 'user', 'secret']);
        
        if(!empty($this->errorService->getResponce())) {
            return $this->errorService->getResponce();
        }
        
        $model = new TwitterUser([
            'name' => $this->params['user'],
        ]);
        
        return ($model->save()) ? $model : ['error' => 'Internal error'];
    }
}
