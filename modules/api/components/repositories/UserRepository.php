<?php

namespace app\modules\api\components\repository;

use app\modules\user\models\User;
use Yii;

/**
 * Description of UserRepository
 *
 * Class ShopCategoryRepository
 * @package app\components\repositories
 */
class UserRepository {

    /**
     * @param string $secret
     * @return yii\db\ActiveRecord|null
     */
    public function getUserBySecret($secret) {
        return User::findOne(['secret' => $secret]);
    }

}
