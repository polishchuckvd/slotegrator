<?php

namespace app\modules\api\components\services;

use Yii;
use yii\web\HttpException;
use yii\web\ForbiddenHttpException;
use app\modules\api\components\repository\UserRepository;
use app\modules\user\models\User;

class ErrorService {

    private $responce = [];

    private $code = null;
    private $params = null;
    
    private $error = [
        401 => 'Access denied',
        400 => 'Missing parameter',
        500 => 'Internal error',
    ];

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function setParams($params) {
        $this->params = $params;
        
        $this->checkParams();
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function getResponce() {
        $this->createResponse();
        
        return $this->responce;
    }

    private function createResponse() {
        $this->checkAccess();

        if (isset($this->code)) {
            $this->responce['error'] = $this->error[$this->code];
        } 
    }

    /**
     * {@inheritdoc}
     */
    private function checkAccess() {
        $user = User::findOne(['secret' => \Yii::$app->request->get('secret')]);

        if (!isset($user)) {
            $this->setCode(401);
        }
    }

    /**
     * {@inheritdoc}
     */
    private function checkParams() {
        $result = [];
        $get = \Yii::$app->request->get();
        
        if(isset($this->params)) {
            foreach ($this->params as $param_key => $param) {
                if(empty($get[$param])) {
                   $result[] =  $param_key;
                }
            }
            
            if(!empty($result)) {
                $this->setCode(400);
            }
        }
    }

}
