<?php

namespace app\modules\api\components\services;

use Yii;
use app\modules\api\models\TwitterApi;

class ApiService {
    
    /**
     * @var TwitterApi
     */
    private $twitterApi;
    
    /**
     * @var string
     */
    private $requestMethod = 'GET';

    /**
     * @var string
     */
    private $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

    /**
     * @var int
     */
    private $count = 5;

    /**
     * @var array
     */
    private $screen_name = [];
    
    /**
     * @var array
     */
    private $twets = [];
    
    public function __construct() {
        $this->twitterApi = new TwitterApi(\Yii::$app->params['twitter']);
    }

    /**
     * @param array $array 
     */
    public function setTwitterNames(array $array) {
        $this->screen_name = $array;
    }
    
    public function getTwets() {
        $result = [];
        
        if(empty($this->screen_name)) {
            throw new Exception('Property $screen_name must be not set!');
        }
        
        foreach ($this->screen_name as $name) {
            $twets = $this->twitterApi->setGetfield('?screen_name=' . $name)
                ->buildOauth($this->url, $this->requestMethod)
                ->performRequest();
             
            $result[] = json_decode($twets);
        }            
        
        return \yii\helpers\ArrayHelper::map($result, 'user.name', 'text', 'id');
    }
    
}
